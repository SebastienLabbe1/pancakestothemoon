#!/usr/bin/env python3
import visualizer, numpy, math, colormap, random
import matplotlib.pyplot as plt


class Tower:
	def __init__(self, n):
		self.n = n
		self.L = [i for i in range(n)]
		self.flips = 0
	
	def shuffle(self):
		random.shuffle(self.L)
	
	def resetCounters(self):
		self.flips = 0

	def get(self, i):
		if i < 0 or i >= self.n:
			exit("WRONG")
		return self.L[i]

	def indexOf(self, x):
		for i,l in enumerate(self.L):
			if x == l:
				return i
		return -1

	def pancakeSize(x):
		return x + 3
	
	def visualize(self):
		vis = visualizer.FasterVisualizer()
		for i,l in enumerate(self.L[::-1]):
			s = Tower.pancakeSize(l)
			vs, fs = visualizer.gen_mesh_cube(numpy.array([0, 0, i]), [s, s, 0.5])
			color = (visualizer.tupcolors[l][0], visualizer.tupcolors[l][1], visualizer.tupcolors[l][2], 1)
			vis.add_mesh(vs, fs, color)
		vis.show_plot()
	
	def flip(self, i):
		#Flip pancakes upto and including the ith pancake
		if i == 0: return #does nothing so just dont flip
		if i < 0 or i >= self.n:
			exit("Tried to flip with index {}".format(i))
		self.L[:i+1] = self.L[i::-1]
		self.flips += 1
	
	def isOrdered(self):
		for i,l in enumerate(self.L):
			if i != l:
				return False
		return True
	
	def getBPNIP(self): #getBiggestPancakeNotInPlace
		imax = -1
		sizemax = -1
		for i in range(self.n-1, -1, -1):
			if self.L[i] > sizemax and self.L[i] != i:
				imax = i
				sizemax = self.L[i]
		return imax
	
	
	def getOri(self, a, b):
		if a < b:
			return 1
		elif b < a:
			return -1
		return 0
	
	def getGroup(self, i):
		if i == -1:
			return -2, -2, -2
		iI = i
		while iI > 0 and abs(self.L[iI] - self.L[iI-1]) == 1:
			iI -= 1
		iE = i
		while iE < self.n-1 and abs(self.L[iE] - self.L[iE+1]) == 1:
			iE += 1
		o = self.getOri(self.L[iI], self.L[iE])
		return iI, iE, o

	def getPRCase(self):
		A = self.getGroup(0)
		B = self.getGroup(self.indexOf(self.L[A[0 if A[2] == 1 else 1]] - 1))
		C = self.getGroup(self.indexOf(self.L[A[1 if A[2] == 1 else 0]] + 1))
		return A, B, C

	def doPrefixReversalSolve(self):
		"""Here is slightly modified version of the prefix reversal algorithm found here 
		https://people.eecs.berkeley.edu/~christos/papers/Bounds%20For%20Sorting%20By%20Prefix%20Reversal.pdf

		in theory this algorithm has a maximum of (5n - 7)/3 steps
		in practice it needs 1.21n steps on average """
		while not self.isOrdered():
			A, B, C = self.getPRCase()
			if A[2] == 0: #top pancake is not in a group
				#try to attach to existing group
				if C[2] == 1 and B[2] == -1: iF = min(C[0], B[0]) - 1
				elif C[2] == 1: iF = C[0] - 1
				elif B[2] == -1: iF = B[0] - 1
				#try to start new group with other lonely pancake
				elif C[2] == 0 and B[2] == 0: iF = min(C[0], B[0]) - 1
				elif C[2] == 0: iF = C[0] - 1
				elif B[2] == 0: iF = B[0] - 1
				#both bigger and smaller pancakes are in groups but impossible to attach
				#thus flip one group to attach to it
				elif B[2] == -2: iF = C[1]
				else: iF = B[1]
			elif A[2] == 1: #top pancake in increasing group
				#try to extend group with smaller pancakes
				if -2 < B[2] < 1: iF = B[0] - 1
				#try to extend group with bigger pancakes (takes 2 moves)
				elif -2 < C[2] < 1: iF = C[1]
				#try to flip current to change group order
				else: iF = A[1]
			elif A[2] == -1: #top pancake in decreasing group
				#try to extend group with bigger pancakes
				if C[2] > -1: iF = C[0] - 1
				#try to extend group with smaller pancakes (takes 2 moves)
				elif B[2] > -1: iF = B[1]
				#try to flip current to change group order
				else: iF = A[1]
			self.flip(iF)
	
	def doRandomSolve(self):
		""" This is a fun way to see that random steps do not work"""
		while not self.isOrdered():
			self.flip(random.randint(1, self.n-1))

	def doNaiveSolve(self):
		"""Here is the naive algorithm which takes the biggest pancake which is not in place
		brings it to the top and then places it correctly"""
		imax = self.getBPNIP()
		while imax != -1:
			self.flip(imax)
			self.flip(self.get(0))
			imax = self.getBPNIP()
	
	def measureSolver(self, solver):
		self.resetCounters()
		self.shuffle()
		solver(self)
		if not self.isOrdered():
			return 1000000
		return self.flips

def plot(X, solvers, nIter = 1):
	flips = [0] * nIter
	Y = [0] * len(X)
	mY = [0] * len(X)
	for solver in solvers:
		print(solver.__name__)
		for it, t in enumerate(X):
			mflips = 0
			print(it, end="\r")
			tower = Tower(t)
			for i in range(nIter):
				f = tower.measureSolver(solver)
				flips[i] = f
				if f > mflips:
					mflips = f
			Y[it] = sum(flips)/nIter
			mY[it] = mflips
		if nIter > 1:
			a, b = numpy.polyfit(X, Y, 1)
			plt.plot(X, Y, 'o')
			plt.plot(X, [a*x + b for x in X], label='{:0.2f}x + {:0.2f} ({} avg)'.format(a, b, solver.__name__))
		am, bm = numpy.polyfit(X, mY, 1)
		plt.plot(X, mY, 'o')
		plt.plot(X, [am*x + bm for x in X], label='{:0.2f}x + {:0.2f} ({} max)'.format(am, bm, solver.__name__))
	plt.legend()
	plt.ylabel('Average number of flips needed')
	plt.xlabel('Number of pancakes')
	plt.title("Pancake algorithms")
	plt.show()

step = 100
maxx = 3000
X = [t for t in range(step, maxx+1, step)]
#plot(X, [Tower.doPrefixReversalSolve, Tower.doNaiveSolve])
plot(X, [Tower.doPrefixReversalSolve])
