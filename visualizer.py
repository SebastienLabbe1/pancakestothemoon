#This code was obtained from Constantin Dresel

"""A module for fast 3d visualization of triangle meshes using vispy."""
import numpy
import vispy.plot
import colormap
import stl

APPROX_ZERO = .00000000000001
"""A constant to use for checking abs of floating point values against zero."""

SCALE_MILLIMETER_TO_SI = 1.0/1000.0
"""A scaling factor to convert millimeters to meters - i.e. to SI units."""

BOX_TRI_INDICES = numpy.array([[1, 2, 3],
                               [0, 1, 3],
                               [0, 3, 4],
                               [0, 1, 4],
                               [1, 4, 5],
                               [1, 2, 5],
                               [2, 5, 6],
                               [2, 3, 6],
                               [3, 6, 7],
                               [5, 6, 7],
                               [4, 5, 7],
                               [3, 4, 7]], dtype=int)
"""Triangle corner indices for a box mesh."""

OCTAHEDRON_TRI_INDICES = numpy.array([[0, 2, 5],
                                      [2, 5, 1],
                                      [5, 1, 3],
                                      [5, 3, 0],
                                      [3, 0, 4],
                                      [0, 4, 2],
                                      [4, 2, 1],
                                      [4, 1, 3]], dtype=int)
"""Triangle corner indices for an octahedron mesh."""

ARROW_TRI_INDICES = numpy.array([[0, 1, 2],  # vertical body
                                 [2, 1, 3],
                                 [7, 8, 9],  # horizontal body
                                 [9, 8, 10],
                                 [4, 5, 6],  # vertical tip
                                 [11, 12, 6]  # horizontal tip
                                 ], dtype=int)
"""Triangle corner indices for an arrow shaped mesh."""


def loadcsv(csvpath, data_type=numpy.float64):
    data = numpy.loadtxt(csvpath, delimiter=',', dtype=data_type)
    size = numpy.asarray(data.shape, dtype=numpy.int32)
    return data, size

def stl_to_npy_file(filename):
    stl_mesh = stl.mesh.Mesh.from_file(filename)
    stl_vects = stl_mesh.vectors
    vectors = stl_vects.reshape(stl_vects.shape[0] * stl_vects.shape[1],
                                stl_vects.shape[2])
    # deduplicate vertices
    vectors_dedup = numpy.unique(vectors, axis=0)
    # find face indices
    faces_flat = numpy.empty(vectors.shape[0], dtype=int)
    for idx, val in enumerate(vectors_dedup):
        min_ok = (vectors >= val - APPROX_ZERO).all(axis=1)
        max_ok = (vectors <= val + APPROX_ZERO).all(axis=1)
        res = min_ok * max_ok
        faces_flat[res] = idx
    faces = numpy.reshape(faces_flat, (int(vectors.shape[0]/vectors.shape[1]
                                           ), vectors.shape[1]))
    # save results to file
    numpy.save(filename[:-4] + "_faces", faces)
    numpy.save(filename[:-4] + "_vecs", vectors_dedup)


def gen_mesh_cube(center, half_len):
    corners = numpy.empty([8, 3], dtype=float)
    corners[0] = center - [-half_len[0], -half_len[1], -half_len[2]]
    corners[1] = center - [-half_len[0], -half_len[1], half_len[2]]
    corners[2] = center - [half_len[0], -half_len[1], half_len[2]]
    corners[3] = center - [half_len[0], -half_len[1], -half_len[2]]
    corners[4] = center - [-half_len[0], half_len[1], -half_len[2]]
    corners[5] = center - [-half_len[0], half_len[1], half_len[2]]
    corners[6] = center - [half_len[0], half_len[1], half_len[2]]
    corners[7] = center - [half_len[0], half_len[1], -half_len[2]]
    return corners, BOX_TRI_INDICES


def gen_mesh_arrow_x(origin, len_to_tip):
    origin = numpy.asarray(origin, dtype=float)
    tip = origin + [len_to_tip, .0, .0]
    corners = numpy.empty([13, 3], dtype=float)
    # body - vertical
    corners[0] = origin + [.0, .0, -.05 * len_to_tip]
    corners[1] = origin + [.0, .0, .05 * len_to_tip]
    corners[2] = origin + .8 * (tip - origin) + [.0, .0, -.05 * len_to_tip]
    corners[3] = origin + .8 * (tip - origin) + [.0, .0, .05 * len_to_tip]
    # tip - vertical
    corners[4] = origin + .8 * (tip - origin) + [.0, .0, -0.09 * len_to_tip]
    corners[5] = origin + .8 * (tip - origin) + [.0, .0, 0.09 * len_to_tip]
    corners[6] = tip
    # body - horizontal
    corners[7] = origin + [.0, -.05 * len_to_tip, .0]
    corners[8] = origin + [.0, .05 * len_to_tip, .0]
    corners[9] = origin + .8 * (tip - origin) + [.0, -.05 * len_to_tip, .0]
    corners[10] = origin + .8 * (tip - origin) + [.0, .05 * len_to_tip, .0]
    # tip - horizontal
    corners[11] = origin + .8 * (tip - origin) + [.0, -0.09 * len_to_tip, .0]
    corners[12] = origin + .8 * (tip - origin) + [.0, 0.09 * len_to_tip, .0]
    return corners, ARROW_TRI_INDICES


def gen_mesh_arrow_y(origin, len_to_tip):
    rot_matrix = numpy.array([[.0, 1.0, .0], [1.0, .0, 0.], [.0, .0, 1.0]])
    verts, tris = gen_mesh_arrow_x(rot_matrix.dot(origin), len_to_tip)
    for vert_idx in range(len(verts)):
        verts[vert_idx] = rot_matrix.dot(verts[vert_idx])
    return verts, tris


def gen_mesh_arrow_z(origin, len_to_tip):
    rot_matrix = numpy.array([[.0, .0, 1.0], [.0, 1.0, 0.], [1.0, .0, .0]])
    verts, tris = gen_mesh_arrow_x(rot_matrix.dot(origin), len_to_tip)
    for vert_idx in range(len(verts)):
        verts[vert_idx] = rot_matrix.dot(verts[vert_idx])
    return verts, tris


def byte_to_signed_int8(byte):
    # heavily inspired by https://stackoverflow.com/a/9147327 (20.12.2018)
    res = byte
    if res & 0x80:  # if sign bit set
        res = res - 0x100
    return res

#List of somewhat different colors taken form internet (link was lost in the abyss sorry)
hexcolors = ["#000000", "#FFFF00", "#FF4A46", "#FF34FF", "#1CE6FF", "#008941", "#006FA6", "#A30059",
        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
        "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
        "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
        "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
        "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
        "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
        "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

        "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
        "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
        "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
        "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"]
tupcolors = [[rgbc/255.0 for rgbc in colormap.hex2rgb(hc)] for hc in hexcolors]

class FasterVisualizer:
    """A class for visualizing meshes in 3d using vispy."""

    def __init__(self):
        self.fig = vispy.plot.Fig(show=False)
        self.axes = self.fig[0, 0]
        self.mesh_verts_list = []
        self.mesh_faces_list = []
        self.mesh_color_list = [] 
        self.lines = []
        self.line_colors = []

    def clear(self):
        self.fig = vispy.plot.Fig(show=False)
        self.axes = self.fig[0, 0]
        self.mesh_verts_list = []
        self.mesh_faces_list = []
        self.mesh_color_list = []
        self.lines = []
        self.line_colors = []

    def add_mesh(self, vects, faces, rgba_color=(.5, .5, .5, 1.0)):
        self.mesh_verts_list.append(vects)
        self.mesh_faces_list.append(faces)
        self.mesh_color_list.append(rgba_color)

    def add_mesh_list(self, vects, faces, rgba_color=(.5, .5, .5, 1.0)):
        assert len(vects) == len(faces), "Lengths of lists to add differ!"
        self.mesh_verts_list += vects
        self.mesh_faces_list += faces
        self.mesh_color_list += ([rgba_color] * len(vects))

    def add_line(self, line, color=(.5, .5, .5, 1.0)):
        self.lines.append(line)
        self.line_colors.append(color)

    def show_plot(self):
        for mesh_verts, mesh_faces, mesh_color in zip(self.mesh_verts_list,
                                                      self.mesh_faces_list,
                                                      self.mesh_color_list):
            self.axes.mesh(numpy.ascontiguousarray(mesh_verts),
                           numpy.ascontiguousarray(mesh_faces),
                           color=mesh_color)
        for line, line_color in zip(self.lines, self.line_colors):
            self.axes.plot(line, color=line_color)
        self.fig.show(run=True)
